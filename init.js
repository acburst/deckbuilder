(function($){
  $(function(){

    // Basic Google Auth
    var currentUser;
    var provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/plus.login');

    var usersRef = firebase.database().ref('users');

    firebase.auth().onAuthStateChanged(function(user) {
      currentUser = user;
      if (user) {
        // User is signed in.
        var username = user.email.split('@')[0];
        renderUser(user);
        usersRef.child(username).once('value', function(snapshot) {
          var val = snapshot.val();
          var onUsernameHand = function(snapshot) {
            var val = snapshot.val();
            if (val.hand) {
              hand = val.hand;
              renderHand(val.hand);
            } else {
              hand = {};
              renderHand({});
            }
          }
          if (!!val) {
            usersRef.child(username).on('value', onUsernameHand);

          } else {
            usersRef.child(username).set({
              name: user.displayName
            }, function() {
              usersRef.child(username).on('value', onUsernameHand);
            });
          }
        });

        // Render Decks
        decksRef.once('value', function(snapshot) {
          var val = snapshot.val();
          var deckRef;
          if (!val) {
            deckRef = decksRef.child('deck1');
            addCardToDeck(deckRef, {
              title: "Test Title",
              text: "Test Text",
              drawn: false
            }, 1);
          }
        });

      } else {
        firebase.auth().signInWithPopup(provider).then(function(result) {
          console.log("SUCCESS");
          // This gives you a Google Access Token. You can use it to access the Google API.
          var token = result.credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          // ...
        }).catch(function(error) {
          console.log("ERROR", error);
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });
      }
    });

    var renderUser = function(user) {
      $('#user').removeClass('hidden');
      $('#user').find('img').attr('src', user.photoURL);
      $('#user').find('.name').text(user.displayName);
    };




    var decks = {};
    var hand = {};

    var decksRef = firebase.database().ref('decks');

    decksRef.on('value', function(snapshot) {
      var val = snapshot.val();
      if (!!val) {
        decks = val;
        $('.library').remove();
        for (var key in val) {
          var library =
            $('<div class="library"><div><h1>'+key+'</h1><input class="NCards" type="number" value="1" placeholder="N"><button class="drawNCards">Draw N cards</button><br></div><div class="card card-creator"><input class="card-title" type="text" placeholder="Insert Title"><textarea class="card-text" type="text" placeholder="Insert Text"></textarea><input class="card-copies" type="text" placeholder="Insert # of copies"><button class="add-card">Add Card</button></div></div>');
          $('body').append(library);
          library.attr('data-id', key);
          var deckRef = decksRef.child(key);
          renderDeck(val[key], deckRef, library);
        }
      }
    });

    var renderCard = function(card, deck, library) {
      var cardEl = $('<div class="card"></div>');
      if (deck[card].drawn) {
        cardEl.addClass('drawn');
      }
      var title = $('<div class="title">' + deck[card].title + '</div>');
      var text = $('<div class="text">' + deck[card].text + '</div>');
      var deleteBut = $('<button class="delete-card">Delete Card</button>');
      cardEl.append(title);
      cardEl.append(text);
      cardEl.append(deleteBut);
      cardEl.attr('data-id', card);
      deleteBut.on('click', function() {
        deleteCardFromDeck(deckRef, $(this).parent().attr('data-id'))
      });
      var cardCreator = library.find('.card-creator');
      if (cardCreator.length) {
        cardCreator.before(cardEl);
      } else {
        library.append(cardEl);
      }
    };

    var renderDeck = function(deck, deckRef, library) {
      library.find('.card:not(.card-creator)').remove();
      for (var card in deck) {
        renderCard(card, deck, library);
      }
    };

    var renderHand = function(hand) {
      var library = $('#my-hand');
      library.empty();
      for (var card in hand) {
        renderCard(card, hand, library);
      }
    };


    var addCardToDeck = function(deckRef, cardObj, cardCopies) {
      // Make sure cardCopies is defined
      if (!cardCopies) {
        cardCopies = 1;
      }

      for (var i=0; i<cardCopies; i++) {
        var newChildRef = deckRef.push();
        newChildRef.set({
          title: cardObj.title,
          text: cardObj.text,
          drawn: false
        });
      };
    };

    // var addCardToDeck = function(deckRef, cardObj) {
    //   var newChildRef = deckRef.push();
    //   newChildRef.set({
    //     title: cardObj.title,
    //     text: cardObj.text
    //   });
    // };

    var deleteCardFromDeck = function(deckRef, cardObj) {
      var cardRef = deckRef.child(cardObj);
      cardRef.remove();
    };


    $(document).on('click', '.add-card', function() {
      var card = $(this).parent();
      var cardObj = {};
      cardObj.title = card.find('.card-title').val();
      cardObj.text = card.find('.card-text').val();
      var cardCopies = card.find('.card-copies').val();

      var deckTitle = $(this).closest('.library').find('h1').text();
      var deckRef = decksRef.child(deckTitle);
      console.log("count: " + cardCopies)
      addCardToDeck(deckRef, cardObj, cardCopies);
    });


    var addDeck = function(decksRef, title) {
      var deckRef = decksRef.child(title);
      addCardToDeck(deckRef, {
        title: "Test Title",
        text: "Test Text",
        drawn: false
      }, 1);
    };

    $('#add-deck').on('click', function() {
      var deckTitle = $('#deck-title').val();
      addDeck(decksRef, deckTitle);
    });


    $(document).on('click', '.drawNCards', function() {
      var n = $(this).prev('.NCards').val();
      if (!n) {
        n = 1;
      }
      var key = $(this).closest('.library').attr('data-id');
      var deck = decks[key];

      for (var i = 0; i < n; i++) {
        var keys = [];
        for (var card in deck) {
          if (!deck[card].drawn) {
            keys.push(card);
          }
        }

        // Can't draw anymore
        if (keys.length === 0) {
          return;
        }

        var randKey = keys[Math.floor(Math.random() * keys.length)];

        deck[randKey].drawn = true;
        hand[randKey] = deck[randKey];
      }

      // Update drawn
      var deckRef = decksRef.child(key);
      deckRef.update(deck);

      // Update hand
      var username = currentUser.email.split('@')[0];
      var userRef = usersRef.child(username);
      userRef.update({
        hand: hand
      })
    });


    $('#resetGame').on('click', function() {
      for (var key in decks) {
        var deck = decks[key];

        for (var card in deck) {
          deck[card].drawn = false;
        }
      }

      // Update drawn
      var deckRef = decksRef.child(key);
      deckRef.update(deck);

      usersRef.once('value', function(snapshot) {
        var users = snapshot.val();
        for (var user in users) {
          delete users[user].hand;
        }
        usersRef.set(users);
      });
    });

  });
})(jQuery); // end of jQuery name space